class Fixnum
  def in_words
    word_str = ""
    hundred_thousands = self % 1000000
    millions = self % 1000000000
    billions = self % 1000000000000
    trillion = self % 1000000000000000
    
    if self >= 1000000000000
      length_tril = trillion.to_s.length
      range_tril = self.to_s[-length_tril..-13].to_i
      word_str += hundred_convertor(range_tril) + " trillion" unless range_tril == 000
      word_str += " " unless self == 1000000000000
    end
    
    if self >= 1000000000
      length_bil = billions.to_s.length
      range_bil = self.to_s[-length_bil..-10].to_i
      word_str += hundred_convertor(range_bil) + " billion" unless range_bil == 000
      word_str += " " if billions > 0 && range_bil != 000
    end
    
    if self >= 1000000
      length = millions.to_s.length
      range = self.to_s[-length..-7].to_i
      word_str += hundred_convertor(range) + " million" unless range == 000
      word_str += " " if millions >= 0 && range != 000
    end
    
    if self >= 1000
      a = hundred_thousands.to_s.length
      b = self.to_s[-a..-4].to_i
      word_str += hundred_convertor(b) + " thousand" unless b == 000
      word_str += " "  if hundred_thousands != 1000 && b != 0 
    end
    
    word_str += hundred_convertor(self)
  end
    
  def hundred_convertor(num)
    word_str = ""
    ones = num % 10
    tens = num % 100
    tens_place = (num % 100) - ones
    hundreds = num % 1000
    if num == 0
      word_str += "zero"
    end
    
    if (100..999).include?(hundreds)
      hundreds_place = hundreds.to_s[0]
      word_str << ones(hundreds_place.to_i) + " hundred"
      if tens > 0
        word_str += " " unless word_str[-1] == " "
      end
    end
    
    case tens_place
      when 20
        word_str += "twenty"
      when 30
        word_str += "thirty"
      when 40
        word_str += "forty"
      when 50
        word_str += "fifty"
      when 60
        word_str += "sixty"
      when 70
        word_str += "seventy"
      when 80
        word_str += "eighty"
      when 90
        word_str += "ninety"
    end
    
    unless ones == 0 || tens <= 19
      word_str += " "  unless word_str[-1] == " "
    end
    
    if (10..19).include?(tens)
      case tens  
      when 10
        word_str += "ten"
      when 11
        word_str += "eleven"
      when 12
        word_str += "twelve"
      when 13
        word_str += "thirteen"
      when 14
        word_str += "fourteen"
      when 15
        word_str += "fifteen"
      when 16
        word_str += "sixteen"
      when 17
        word_str += "seventeen"
      when 18
        word_str += "eighteen"
      when 19
        word_str += "nineteen"
      end
    else
      word_str += ones(num)
    end
      word_str
  end
    
  def ones(num)
    ones = num % 10
    word_str = ""
      case ones
      when 1
        word_str += "one"
      when 2
        word_str += "two"  
      when 3
        word_str += "three"
      when 4
        word_str += "four"
      when 5
        word_str += "five"
      when 6
        word_str += "six"
      when 7
        word_str += "seven"  
      when 8
        word_str += "eight"
      when 9
        word_str += "nine"
      end
    word_str
  end
end